import React, { Component } from 'react'
import {
    Text,
    StyleSheet,
    View,
    Dimensions,
    Image,
    Animated,
    PanResponder,
    TouchableOpacity
} from 'react-native'

import AsyncStorage from "@react-native-community/async-storage";

import Users from "../assets/datas/demo";
import Info from "../assets/datas/info";

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentIndex: 0,
            info: Info,
            title: "My address is",
            body: Users[0].address
        };
        this.position = new Animated.ValueXY();

        this.rotate = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: ['-10deg', '0deg', '10deg'],
            extrapolate: 'clamp'
        });

        this.rotateAndTranslate = {
            transform: [{
                rotate: this.rotate
            },
            ...this.position.getTranslateTransform()
            ]
        };

        this.nextCardOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0, 1],
            extrapolate: 'clamp'
        });

        this.nextCardScale = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0.8, 1],
            extrapolate: 'clamp'
        })
    }

    _onPressTitle = (item, i, index) => {
        Info.map((e) => {
            if (item.id === e.id) {
                return { ...e.isChosse = true }
            } else {
                return {
                    ...e.isChosse = false,
                }
            }
        })
        this.setState({ info: Info })
        switch (i + 1) {
            case 1:
                this.setState({ title: "My name is", body: Users[index].name });
                break;
            case 2:
                this.setState({ title: "My birthday is", body: Users[index].birthday });
                break;
            case 3:
                this.setState({ title: "My address is", body: Users[index].address });
                break;
            case 4:
                this.setState({ title: "My phone is", body: Users[index].phone });
                break;
            case 5:
                this.setState({ title: "Looked", body: "" });
                break;
            default:
                null
        }
    }

    componentWillMount() {
        this.PanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onPanResponderMove: (evt, gestureState) => {
                this.position.setValue({ x: gestureState.dx, y: gestureState.dy })
            },
            onPanResponderRelease: async (evt, gestureState) => {
                if (gestureState.dx > 120) {
                    try {
                        const productToBeSaved = Users[this.state.currentIndex];
                        const existingProducts = await AsyncStorage.getItem('listUserLiked');
                        let newProduct = JSON.parse(existingProducts);
                        if (!newProduct) {
                            newProduct = []
                        }
                        newProduct.push(productToBeSaved)

                        await AsyncStorage.setItem('listUserLiked', JSON.stringify(newProduct))
                            .then(() => {
                                console.log('It was saved successfully')
                            })
                            .catch(() => {
                                console.log('There was an error saving the product')
                            })
                        console.log(Users[this.state.currentIndex])
                    } catch (e) {
                        console.log(e)
                    }

                    Animated.spring(this.position, {
                        toValue: { x: SCREEN_WIDTH + 100, y: gestureState.dy }
                    }).start(() => {
                        this.setState({ currentIndex: this.state.currentIndex + 1 }, () => {
                            this.position.setValue({ x: 0, y: 0 })
                        })
                    })
                } else if (gestureState.dx < -120) {
                    Animated.spring(this.position, {
                        toValue: { x: -SCREEN_WIDTH - 100, y: gestureState.dy }
                    }).start(async () => {
                        this.setState({ currentIndex: this.state.currentIndex + 1 }, () => {
                            this.position.setValue({ x: 0, y: 0 })
                        });

                    })
                } else {
                    Animated.spring(this.position, {
                        toValue: { x: 0, y: 0 },
                        friction: 4
                    }).start()
                }
            }
        })
    }

    renderInfo = (index) => {
        return this.state.info.map((item, i) => {
            return (
                <TouchableOpacity
                    onPress={() => this._onPressTitle(item, i, index)}
                >
                    <View>
                        <Image
                            style={{
                                width: 50,
                                height: 50,
                                tintColor: item.isChosse ? 'green' : null
                            }}
                            source={item.uri}

                        />
                    </View>

                </TouchableOpacity>
            )
        })
    }

    renderUsers = () => {
        return Users.map((item, index) => {
            if (index < this.state.currentIndex) {
                return null
            } else if (index == this.state.currentIndex) {
                return (
                    <Animated.View
                        {...this.PanResponder.panHandlers}
                        key={item.id} style={[this.rotateAndTranslate, styles.viewContent
                        ]}>
                        <View style={styles.contentTop}>
                            <View style={styles.viewAvatar}>
                                <Image
                                    style={styles.avatar}
                                    source={{ uri: item.avatar }}
                                />
                            </View>

                        </View>
                        <View style={styles.contentBottom}>
                            <View style={styles.topContent}>
                                <Text style={styles.textTitle}>
                                    {this.state.title}
                                </Text>
                                <Text style={styles.textBody}>
                                    {this.state.body}
                                </Text>
                            </View>
                            <View style={styles.viewInfo}>
                                {this.renderInfo(index)}
                            </View>

                        </View>
                    </Animated.View>
                )
            } else {
                return (
                    <Animated.View
                        {...this.PanResponder.panHandlers}
                        key={item.id} style={[this.rotateAndTranslate, styles.viewContent
                        ]}>
                        <View style={styles.contentTop}>
                            <View style={styles.viewAvatar}>
                                <Image
                                    style={styles.avatar}
                                    source={{ uri: item.avatar }}
                                />
                            </View>

                        </View>
                        <View style={styles.contentBottom}>
                            <View style={styles.topContent}>
                                <Text style={styles.textTitle}>
                                    {this.state.title}
                                </Text>
                                <Text style={styles.textBody}>
                                    {this.state.body}
                                </Text>
                            </View>
                            <View style={styles.viewInfo}>
                                {this.renderInfo(index)}
                            </View>

                        </View>
                    </Animated.View>
                )
            }

        }).reverse()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Liked')}
                    >
                        <Image
                            style={styles.iconLiked}
                            source={require('../assets/icons/heart.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    {this.renderUsers()}
                </View>
                <View style={{ height: 160 }}>

                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        height: 100,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    iconLiked: {
        width: 50,
        height: 50,
        marginLeft: 20,
        marginTop: 10
    },
    viewContent: {
        height: SCREEN_HEIGHT - 260,
        width: SCREEN_WIDTH - 40,
        margin: 20,
        position: 'absolute',
        borderWidth: 0.5,
        borderColor: 'grey'
    },
    contentTop: {
        flex: 1,
        borderBottomWidth: 0.5,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    contentBottom: {
        flex: 2,
        justifyContent: 'space-between',
        padding: 25
    },
    viewInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewAvatar: {
        width: 150,
        height: 150,
        borderRadius: 75,
        padding: 30,
        borderWidth: 0.5,
        borderColor: 'grey',
        zIndex: 1000,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: -50,
        backgroundColor: 'white'
    },
    avatar: {
        width: 140,
        height: 140,
        borderRadius: 70,
    },
    topContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textTitle: {
        fontSize: 23,
        color: 'grey',
        marginBottom: 30
    },
    textBody: {
        fontSize: 28,
        fontWeight: 'bold',
        color: 'black'
    }
})
