import React, { Component } from 'react'
import {
    Text,
    StyleSheet,
    View,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native'
import AsyncStorage from "@react-native-community/async-storage";

export default class LikedScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listUserLiked: []
        };
    }

    async componentDidMount() {
        try {
            const existingProducts = await AsyncStorage.getItem('listUserLiked');
            let listUserLiked = JSON.parse(existingProducts);
            if (!listUserLiked) {
                listUserLiked = []
            }
            this.setState({ listUserLiked })
            console.log(listUserLiked)
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        if (this.state.listUserLiked.length < 1) {
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Image
                                style={styles.iconBack}
                                source={require('../assets/icons/left-arrow.png')}
                            />
                        </TouchableOpacity>

                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Image
                                style={styles.iconBack}
                                source={require('../assets/icons/left-arrow.png')}
                            />
                        </TouchableOpacity>

                    </View>
                    <View style={styles.content}>
                        <FlatList
                            data={this.state.listUserLiked}
                            extraData={this.state}
                            numColumns={2}
                            keyExtractor={(item, index) => index + ''}
                            renderItem={({ item, index }) => {
                                return (
                                    <View style={styles.content}>
                                        <Image
                                            style={styles.avatar}
                                            source={{ uri: item.avatar }}
                                        />
                                        <Text>Name: {item.name}</Text>
                                        <Text>Birthday: {item.birthday}</Text>
                                        <Text>Location: {item.address}</Text>
                                        <Text>Phone: {item.phone}</Text>
                                    </View>
                                )
                            }}
                        />
                    </View>
                </View>
            )
        }

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: 90,
        borderBottomWidth: 0.5,
        borderColor: 'grey',
        justifyContent: 'center'
    },
    content: {
        flex: 1
    },
    iconBack: {
        width: 50,
        height: 50,
        marginLeft: 20,
        marginTop: 10
    },
    avatar: {
        height: 100,
        width: null
    }
})
