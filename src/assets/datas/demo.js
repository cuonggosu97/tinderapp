module.exports = [
    {
        id: '1',
        name: 'David',
        birthday: '03/02/1998',
        address: 'Nguyễn Trãi',
        phone: '0965963485',
        avatar: 'https://gitlab.com/cuonggosu97/tinderapp/-/raw/ef993289c9e442a5272aeced8be992070a97dce6/assets/01.jpg'
    },
    {
        id: '2',
        name: 'Josh',
        birthday: '03/02/4531',
        address: 'Nguyễn Trãi',
        phone: '0965931855',
        avatar: 'https://gitlab.com/cuonggosu97/tinderapp/-/raw/ef993289c9e442a5272aeced8be992070a97dce6/assets/02.jpg'
    },
    {
        id: '3',
        name: 'Medusa',
        birthday: '03/02/7831',
        address: 'Nguyễn Trãi',
        phone: '0965213855',
        avatar: 'https://gitlab.com/cuonggosu97/tinderapp/-/raw/ef993289c9e442a5272aeced8be992070a97dce6/assets/03.jpg'
    },
    {
        id: '4',
        name: 'Luffy',
        birthday: '03/02/6453',
        address: 'Nguyễn Trãi',
        phone: '0966781855',
        avatar: 'https://gitlab.com/cuonggosu97/tinderapp/-/raw/ef993289c9e442a5272aeced8be992070a97dce6/assets/04.jpg'
    },
    {
        id: '5',
        name: 'Sanji',
        birthday: '03/02/1997',
        address: 'Nguyễn Trãi',
        phone: '09631261855',
        avatar: 'https://gitlab.com/cuonggosu97/tinderapp/-/raw/ef993289c9e442a5272aeced8be992070a97dce6/assets/05.jpg'
    },
]