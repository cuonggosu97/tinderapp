module.exports = [
    {
        id: '1',
        uri: require('../icons/user.png'),
        isChosse: false
    },
    {
        id: '2',
        uri: require('../icons/calendar.png'),
        isChosse: false
    },
    {
        id: '3',
        uri: require('../icons/location.png'),
        isChosse: true
    },
    {
        id: '4',
        uri: require('../icons/phone.png'),
        isChosse: false
    },
    {
        id: '5',
        uri: require('../icons/lock.png'),
        isChosse: false
    },
]