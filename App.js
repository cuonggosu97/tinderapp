import React, { Component } from 'react'
import {
  View,
} from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import "react-native-gesture-handler";

import HomeScreen from "./src/HomeScreen";
import LikedScreen from "./src/LikedScreen";

const Stack = createStackNavigator();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };;
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: 'white'
      }}>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName='Home'
            headerMode={'none'} >
            <Stack.Screen
              name='Home'
              component={HomeScreen}
            // options={{ gestureEnabled: true, gestureDirection: 'horizontal' }}
            />
            <Stack.Screen
              name='Liked'
              component={LikedScreen}
              options={{ gestureEnabled: true, gestureDirection: 'horizontal' }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    )
  }
}

export default App
